import java.util.*;

public class CollectionExercises {

    public Integer sumList(List<Integer> list) {
        return 0;
    }

    public List<Integer> slowCollection() {
        // returns a list with 100.000 integers
        ArrayList<Integer> collection = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            // add item to beginning of list (at index 0)
            collection.add(0, i);
        }
        return collection;
    }

    public List<Integer> filterOutValuesLowerThan5(List<Integer> list) {
        return list;
    }

    public List<Integer> filterOutOddNumbers(List<Integer> list) {
        return list;
    }

    public int findIndexOf(String item, List<String> list) {
        return 0;
    }

    public List<String> reverseList(List<String> list) {
        return list;
    }

    public List<Integer> sortList(List<Integer> list) {
        return list;
    }

    public ArrayList<String> applesAndPears() {

        ArrayList<String> collection = new ArrayList<>();
        collection.add("Apple");
        collection.add("Apple");
        collection.add("Pear");
        collection.add("Pear");
        collection.add("Pear");

        return collection;
    }

    public Map<String, Integer>createHashMap(String[] keys, Integer[] values) {
        Map<String, Integer> result = new HashMap<>();

        return result;
    }
}
