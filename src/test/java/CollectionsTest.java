import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CollectionsTest {

    CollectionExercises collections = new CollectionExercises();

    @Test
    public void testSum() {
        // calculate the total of all the values in the list
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(6);
        list.add(19);
        list.add(4);
        list.add(12);
        list.add(6);

        Integer sum = collections.sumList(list);
        assertEquals(49, (int) sum);
    }

    @Test
    public void testFilter1() {
        // filter out all the values lower than 5 from the list
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(9);
        list.add(12);
        list.add(6);
        list.add(3);
        list.add(7);

        List<Integer> newList = collections.filterOutValuesLowerThan5(list);

        for (Integer i : newList) {
            assertTrue(i.compareTo(5) > 0);
        }
    }

    @Test
    public void testFilter2() {
        // filter out all the odd numbers from the list
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(4);
        list.add(3);
        list.add(7);
        list.add(12);
        list.add(9);

        List<Integer> newList = collections.filterOutOddNumbers(list);

        for (Integer i : newList) {
            assertEquals(0, i % 2);
        }
    }

    @Test
    public void testFindIndexOf() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Jan");
        list.add("Piet");
        list.add("Klaas");
        list.add("Bouke");

        int index = collections.findIndexOf("Klaas", list);
        assertEquals(index, 2);
    }

    @Test
    public void testReverse() {
        // reverse
        List<String> list = new ArrayList<>();
        list.add("Teen");
        list.add("Knie");
        list.add("Schouders");
        list.add("Hoofd");

        List<String> reversedList = collections.reverseList(list);
        assertEquals("Hoofd", reversedList.get(0));
        assertEquals("Teen", reversedList.get(3));

        // bonus points: considering List is a interface and not a
        // concrete class, could you swap it out for a different
        // collection type than ArrayList?
    }

    @Test
    public void testSort() {
        // reverse
        List<Integer> list = new LinkedList<>();
        list.add(5);
        list.add(9);
        list.add(0);
        list.add(4);

        List<Integer> reversedList = collections.sortList(list);
        assertEquals(0, (int) reversedList.get(0));
        assertEquals(9, (int) reversedList.get(3));

        // bonus points: considering List is a interface and not a
        // concrete class, could you swap it out for a different
        // collection type than LinkedList?
    }

    @Test
    public void testApplesAndPears() {
        // this collection contains some duplicates.
        // which collection type can we use to enforce uniqueness of every element?
        int collectionSize = collections.applesAndPears().size();
        assertEquals(collectionSize, 2);
    }

    @Test
    public void testSlowCollection() {
        // this function executes quite slow (~1000 ms)
        // can we use a different collection type to make it faster?
        long start = System.currentTimeMillis();
        collections.slowCollection();
        long end = System.currentTimeMillis();
        long executionTime = end - start;
        System.out.println("function executed in " + executionTime + " milliseconds");
        assertTrue(executionTime < 200);
    }

    @Test
    public void testHashMap() {
        // this function should marshal the two input arrays into a HashMap representation
        // where the first array represents the keys and the second the values.
        // please provide the implementation
        String[] cities = { "Amsterdam", "Rotterdam", "Amersfoort", "Groningen" };
        Integer[] temperatures = { 20, 20, 19, 16 };
        Map<String, Integer> weatherData = collections.createHashMap(cities, temperatures);

        assertEquals(20, (int) weatherData.get("Amsterdam"));
        assertEquals(20, (int) weatherData.get("Rotterdam"));
        assertEquals(19, (int) weatherData.get("Amersfoort"));
        assertEquals(16, (int) weatherData.get("Groningen"));

        // bonus points: can you make the createHashMap function
        // generic over the input types (i.e. String & Integer)
    }

//    public void testSortStrings() {
//        // make the sortList method generic over the input
//        // type, so it works with both Strings and Integers
//        List<String> list = new ArrayList<String>();
//        list.add("Zaandam");
//        list.add("Amersfoort");
//        list.add("Rotterdam");
//        list.add("Amsterdam");
//        list.add("Groningen");
//
//        // the following lines are commented out because
//        // the source needs to be changed before it will
//        // compile
//        List<String> reversedList = collections.sortList(list);
//        assertTrue(reversedList.get(0).equals("Amersfoort"));
//        assertTrue(reversedList.get(4).equals("Zaandam"));
//
//        // dummy test to force test to fail
//        assertTrue(false);
//    }
}