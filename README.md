# Java Collections exercises

This project contains two classes:
* `CollectionExcercises`: the functions in this class must be implemented as an exercise.
* `CollectionsTest`: tests that prove the implementation is correct.

Run the tests defined in `CollectionTests` to see which functions still need to be implemented.
Try to provide an implementation for each of the functions in `CollectionExercises`.

Good luck!
